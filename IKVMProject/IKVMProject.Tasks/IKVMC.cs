﻿/*
  Copyright (C) 2012 Volker Berlin (i-net software)
  Copyright (C) 2012 Jeroen Frijters
  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.
  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:
  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
  Jeroen Frijters
  jeroen@frijters.net

*/

using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System;
using System.IO;
using System.Linq;

namespace IKVMProject
{
    /// <summary>
    /// IKVM Compiler Task
    /// </summary>
    public sealed class IKVMC : ToolTask
    {
        /// <summary>
        /// Gets or sets the output assembly type.
        /// </summary>
        public string Configuration { get; set; }

        /// <summary>
        /// Gets or sets ikvm home path
        /// </summary>
        public string IKVMHome { get; set; }

        /// <summary>
        /// Gets or sets the source files that will be compiled.
        /// </summary>
        public ITaskItem[] Libraries { get; set; }

        /// <summary>
        /// Gets or sets the class with the main method.
        /// </summary>
        public string MainClass { get; set; }

        /// <summary>
        /// Gets or sets the output path.
        /// </summary>
        public string OutputPath { get; set; }

        /// <summary>
        /// Gets or sets the platform that will be targeted by the compiler (e.g. x86).
        /// </summary>
        public string Platform { get; set; } = "x86";

        /// <summary>
        /// References to Assemblies, SDK and Projects
        /// </summary>
        public ITaskItem[] References { get; set; }

        /// <summary>
        /// Resources
        /// </summary>
        public ITaskItem[] Resources { get; set; }

        /// <summary>
        /// Gets or sets the output assembly type.
        /// </summary>
        public string TargetType { get; set; }

        protected override string ToolName => "ikvmc.exe";

        protected override string GenerateCommandLineCommands()
        {
            CommandLineBuilder commandLine = new CommandLineBuilder();

            // Always generate .pdb files
            commandLine.AppendSwitch("-debug");

            commandLine.AppendSwitchIfNotNull("-out:", OutputPath);

            commandLine.AppendSwitchIfNotNull("-target:", TargetType?.ToLower());

            commandLine.AppendSwitch("-reference:mscorlib.dll");
            commandLine.AppendSwitch("-reference:System.dll");

            foreach (var item in References)
            {
                commandLine.AppendSwitchIfNotNull("-reference:", item);
            }

            Array.Sort(Libraries, (x, y) =>
            {
                int xOrder = 0;
                int yOrder = 0;

                bool xHas = x.MetadataNames.Cast<string>().Any(c => c == "Order");
                bool yHas = y.MetadataNames.Cast<string>().Any(c => c == "Order");

                if (xHas)
                    int.TryParse(x.GetMetadata("Order"), out xOrder);

                if (yHas)
                    int.TryParse(y.GetMetadata("Order"), out yOrder);

                return xOrder.CompareTo(yOrder);
            });
            commandLine.AppendFileNamesIfNotNull(Libraries, " ");

            //Log.LogWarning(commandLine.ToString(), null);
            return commandLine.ToString();
        }

        protected override string GenerateFullPathToTool() => Path.Combine(IKVMHome, ToolName);
    }
}
