﻿/***************************************************************************

Copyright (c) Microsoft Corporation. All rights reserved.
This code is licensed under the Visual Studio SDK license terms.
THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

***************************************************************************/

namespace IKVMProject
{
	using Microsoft.VisualStudio.ProjectSystem;
	using Microsoft.VisualStudio.ProjectSystem.Designers;
	using Microsoft.VisualStudio.ProjectSystem.Utilities;
	using Microsoft.VisualStudio.Shell;
	using Microsoft.VisualStudio.Shell.Interop;
	using System.ComponentModel.Composition;

	[Export]
	[AppliesTo(IKVMProjectUnconfigured.UniqueCapability)]
	[ProjectTypeRegistration(VsPackage.ProjectTypeGuid, "IKVMProject", "#2", ProjectExtension, Language, resourcePackageGuid: VsPackage.PackageGuid, PossibleProjectExtensions = ProjectExtension, ProjectTemplatesDir = @"..\..\Templates\Projects\MyCustomProject")]
	[ProvideProjectItem(VsPackage.ProjectTypeGuid, "My Items", @"..\..\Templates\ProjectItems\MyCustomProject", 500)]
	internal class IKVMProjectUnconfigured
	{
		internal const string Language = "IKVMProject";
		/// <summary>
		/// The file extension used by your project type.
		/// This does not include the leading period.
		/// </summary>
		internal const string ProjectExtension = "ikvmproj";

		/// <summary>
		/// A project capability that is present in your project type and none others.
		/// This is a convenient constant that may be used by your extensions so they
		/// only apply to instances of your project type.
		/// </summary>
		/// <remarks>
		/// This value should be kept in sync with the capability as actually defined in your .targets.
		/// </remarks>
		internal const string UniqueCapability = "IKVMProject";

		[Import]
		internal ActiveConfiguredProject<ConfiguredProject> ActiveConfiguredProject { get; }

		[Import]
		internal ActiveConfiguredProject<IKVMProjectConfigured> MyActiveConfiguredProject { get; }

		[ImportMany(ExportContractNames.VsTypes.IVsProject, typeof(IVsProject))]
		internal OrderPrecedenceImportCollection<IVsHierarchy> ProjectHierarchies { get; }

		internal IVsHierarchy ProjectHierarchy
		{
			get { return this.ProjectHierarchies.Single().Value; }
		}

		[Import]
		internal IActiveConfiguredProjectSubscriptionService SubscriptionService { get; }

		[Import]
		internal IThreadHandling ThreadHandling { get; }

		[Import]
		internal UnconfiguredProject UnconfiguredProject { get; }

		[ImportingConstructor]
		public IKVMProjectUnconfigured(UnconfiguredProject unconfiguredProject)
		{
			this.ProjectHierarchies = new OrderPrecedenceImportCollection<IVsHierarchy>(projectCapabilityCheckProvider: unconfiguredProject);
		}
	}
}
