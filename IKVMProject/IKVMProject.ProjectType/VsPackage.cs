﻿/***************************************************************************

Copyright (c) Microsoft Corporation. All rights reserved.
This code is licensed under the Visual Studio SDK license terms.
THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.

***************************************************************************/

namespace IKVMProject
{
	using Microsoft.VisualStudio.Shell;
	using System;
	using System.ComponentModel;
	using System.Runtime.InteropServices;

	/// <summary>
	/// This class implements the package exposed by this assembly.
	/// </summary>
	/// <remarks>
	/// This package is required if you want to define adds custom commands (ctmenu)
	/// or localized resources for the strings that appear in the New Project and Open Project dialogs.
	/// Creating project extensions or project types does not actually require a VSPackage.
	/// </remarks>
	[PackageRegistration(UseManagedResourcesOnly = true)]
	[Description("A custom project type based on CPS")]
	[Guid(VsPackage.PackageGuid)]
	public sealed class VsPackage : Package
	{
		/// <summary>
		/// The GUID for this package.
		/// </summary>
		public const string PackageGuid = "24adbeba-ac11-4fd8-8c84-023befef61db";

		/// <summary>
		/// The file extension of this project type.  No preceding period.
		/// </summary>
		public const string ProjectExtension = "ikvmproj";
		/// <summary>
		/// The GUID for this project type.  It is unique with the project file extension and
		/// appears under the VS registry hive's Projects key.
		/// </summary>
		public const string ProjectTypeGuid = "d1f43312-1eb5-4f7b-a25b-c7ea1d81cd9a";
		/// <summary>
		/// The default namespace this project compiles with, so that manifest
		/// resource names can be calculated for embedded resources.
		/// </summary>
		internal const string DefaultNamespace = "IKVMProject";
	}
}
